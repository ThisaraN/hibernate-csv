package com.example.customerapi.controller;

import com.example.customerapi.model.Customer;
import com.example.customerapi.repository.AddressRepository;
import com.example.customerapi.repository.CustomerRepository;
import org.hibernate.HibernateException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/api")
public class DriverController {
    @Autowired
    CustomerRepository customerRepository;
    AddressRepository addressRepository;

    @PersistenceContext
    private EntityManager em;
//
//    @PostMapping("/customer")
//    public Customer createDriver(@Valid
//                                 @RequestBody Customer customer) {
//        return customerRepository.save(customer);
//    }

    @GetMapping("/customer")
    public List<Customer> getAllCustomers() {
        List<Customer> resultList = new ArrayList<>();
        try {
            Query query = em.createNativeQuery("SELECT * FROM Customer ");
            resultList = (List<Customer>) query.getResultList();
        } catch (HibernateException e) {
            e.printStackTrace();
        } finally {
            em.close();
        }

        return resultList;
    }

    /*
     * public void getDriverFromFirebaseDatabase() {
     *
     *   String uri = "https://smarttraveler-6b8f0.firebaseio.com/Users/UUoDWMbTL6UmbgEbjjE1iJzJXqw2.json";
     *   RestTemplate restTemplate = new RestTemplate();
     *   Driver driver = restTemplate.getForObject(uri, Driver.class);
     *   String name = driver.getDriverName();
     *   System.out.println(name);
     * }
     */
}


//~ Formatted by Jindent --- http://www.jindent.com
